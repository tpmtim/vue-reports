const fs = require('fs');
const path = require('path');

function saveReport(report, folderPath) {
  fs.writeFileSync(folderPath + '/report.json', JSON.stringify(report), 'utf8');

  copyRecursiveSync(
    path.resolve(__dirname, '../../standalone-reporter/'),
    folderPath
  );
}

/**
 * Look ma, it's cp -R.
 * @param {string} src The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 */
const copyRecursiveSync = function(src, dest) {
  const exists = fs.existsSync(src);
  const stats = exists && fs.statSync(src);
  const isDirectory = exists && stats.isDirectory();
  if (exists && isDirectory) {
    if (!exists) fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(
        path.join(src, childItemName),
        path.join(dest, childItemName)
      );
    });
  } else {
    if (fs.existsSync(dest)) {
      fs.unlinkSync(dest);
    }
    fs.linkSync(src, dest);
  }
};

module.exports = { saveReport };
