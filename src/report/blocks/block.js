class Block {
  constructor(title) {
    if (!title) throw new error('No Title definied for Block');
    this.title = title;
    this.blockType = '';
    this.expanded = true;
  }
}

module.exports = Block;
