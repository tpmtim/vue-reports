const Block = require('./block');

class ListBlock extends Block {
  constructor(title, testedUrl, goldenImage, testImage, diffImage, diffAreas) {
    super(title);
    this.blockType = 'image_diff';

    this.url = testedUrl;

    this.goldenImage = goldenImage;
    this.testImage = testImage;
    this.diffImage = diffImage || '';

    this.isDifferent = this.diffImage !== '' || false;
    this.expanded = this.isDifferent;
    if (this.isDifferent) {
      this.icon = 'warning';
    } else {
      this.icon = 'success';
    }

    this.diffAreas = diffAreas || [];
  }
}

module.exports = ListBlock;
