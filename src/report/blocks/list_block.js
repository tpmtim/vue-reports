const Block = require('./block');

class ListBlock extends Block {
  constructor() {
    super();
    this.blockType = 'list';
  }
}

module.exports = ListBlock;
