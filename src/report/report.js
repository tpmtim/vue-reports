class Report {
  constructor(title) {
    this.title = title;
    this.blocks = [];
  }

  addBlock(block) {
    this.blocks.push(block);
  }
}

module.exports = Report;
