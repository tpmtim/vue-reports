import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import VueReport from './vue_report.vue';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);

fetch('report.json')
  .then(response => response.json())
  .then(rep => {
    const vueComp = new Vue({
      data: {
        report: rep
      },
      render: function(create) {
        return create(VueReport, {
          props: {
            report: this.report
          }
        });
      }
    }).$mount('#reportcontainer');
  })
  .catch(ex => {
    console.error('Failed loading the report: ', ex);
  });
