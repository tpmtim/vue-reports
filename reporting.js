const Report = require('./src/report/report');
const ReportSaver = require('./src/report/report_saver');

const ImageDiffBlock = require('./src/report/blocks/image_diff_block');
const ListBlock = require('./src/report/blocks/list_block');

module.exports = { Report, ReportSaver, ListBlock, ImageDiffBlock };
